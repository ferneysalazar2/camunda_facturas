package com.solinfo.factura.facturabpm;

import org.camunda.bpm.application.ProcessApplication;
import org.camunda.bpm.application.impl.ServletProcessApplication;

@ProcessApplication("Factura BPM")
public class FacturaApplication extends ServletProcessApplication {
  // empty implementation
}
